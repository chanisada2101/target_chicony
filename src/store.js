import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    biz: "0",
  },
  mutations: {
    CHANGE_BIZ(state, biz) {
      state.biz = biz;
    },
  },
  actions: {
    changeBiz({ commit }, biz) {
      commit('CHANGE_BIZ', biz);
    },
  },
  getters: {
    bizSelect: state => {
      return state.biz
    },
  },
})
