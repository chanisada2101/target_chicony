import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Line1 from './views/Line1.vue'
import Line2 from './views/Line2.vue'
import Line3 from './views/Line3.vue'
import Line4 from './views/Line4.vue'
import Line5 from './views/Line5.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      name: 'wildcard',
      redirect: '/'
    },
    {
      path: '/',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/line1',
      name: 'Line1',
      component: Line1
    },
    {
      path: '/line2',
      name: 'Line2',
      component: Line2
    },
    {
      path: '/line3',
      name: 'Line3',
      component: Line3
    },
    {
      path: '/line4',
      name: 'Line4',
      component: Line4
    },
    {
      path: '/line5',
      name: 'Line5',
      component: Line5
    },
  ]
})
export default router
